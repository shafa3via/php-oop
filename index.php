<?php
    require("animal.php");
    
$sheep = new Animal("Shaun");

//cara tulisnya $(nama objek)->(property)
//pengisian properti diatas tdk usah pakai $

echo "Name : " . $sheep->get_name() . '<br>'; // "shaun"
echo "Legs : " . $sheep->legs . '<br>'; // 4
echo "Cold-blooded : " . $sheep->cold_blooded . '<br><br>'; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
require("ape.php");
require("frog.php");
$kodok = new Frog("buduk");
echo "Name : " . $kodok->get_name() . '<br>'; // "shaun"
echo "Legs : " . $kodok->legs . '<br>'; // 4
echo "Cold-blooded : " . $kodok->cold_blooded .'<br>'; // "no"
echo "Jump : ";
$kodok->jump();
echo '<br><br>'; // "hop hop"

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->get_name() . '<br>'; // "shaun"
echo "Legs : " . $sungokong->legs . '<br>'; // 4
echo "Cold-blooded : " . $sungokong->cold_blooded . '<br>'; // "no"
echo "Yell : "; 
$sungokong->yell();
echo '<br><br>'; // "Auooo"

?>