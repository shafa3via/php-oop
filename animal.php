<?php
    class Animal{

        //property harus pake modifier
        public $legs = 4;
        public $cold_blooded = "No";
        public $name;
        
        //constructor
        public function __construct($name){
            $this->name = $name;
            }

        //method boleh/tdk pake modifier
        //metode set dan get       
        public function set_name($name){
            $this->name = $name;
        }
        public function get_name(){
            return $this->name;
            //penulisan $this->property tanpa $
        }    
    }

?>